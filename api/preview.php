<?php
error_reporting(0);
header('Content-Type: application/json');

$return['title'] = "";
$return['image'] = "https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg";

if (isset($_GET["url"])) {
    $url = $_GET["url"];
    $tags = get_meta_tags($url);
    $return = [];
    $return['title'] = isset($tags["twitter:title"]) ? $tags["twitter:title"] : "";
    $return['image'] = isset($tags["twitter:image"]) ? $tags["twitter:image"] : "https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg";
}
die(json_encode($return));
?>