$(document).ready(function () {
    jobs = [];

    $("#supp").click(function (e) {
        e.preventDefault();
        $("#supported-sites").slideDown();
    });

    $("#url").change(function (e) {
        e.preventDefault();
        data = getArticlesData($("#url").val());
        $("#article-image").attr("src", data.image);
        $("#article-title").html(data.title);
    });

    function getArticlesData(url) {
        ret = {};
        $.ajax({
            type: "GET",
            url: "api/preview.php",
            async: false,
            data: {url: url},
            success: function (response) {
                ret = response;
            }
        });
        return ret;
    }

    $("#addJob").submit(function (e) {
        e.preventDefault();
        url = $("#url").val();
        var settings = {
            "url": "http://localhost:8086/upload/start?articleLink=" + encodeURI(url),
            "method": "POST",
            "timeout": 0,
        };

        $.ajax(settings).done(function (response) {
            n = parseInt(response);
            if (Number.isInteger(n)) {
                data = getArticlesData(url);
                jobs.push({id: n, status: "STARTED", title: data.title});
                updateTable();
            } else {
                alert("Invalid response");
            }
        });
    });

    function getJobsData(id) {
        resp = {};
        $.ajax({
            type: "GET",
            url: "http://localhost:8086/jobs",
            data: {id: id},
            async: false,
            success: function (response) {
                resp = response;
            }
        });
        return resp;
    }

    function updateTable() {
        $("#tbody").html("");
        for (i = 0; i < jobs.length; i++) {
            jobdata = getJobsData(jobs[i].id)
            jobs[i].status = jobdata.JOB_GENERAL_STATUS;
            jobs[i].detailedStatus = jobdata.JOB_DETAILED_STATUS
            jobs[i].videoId = jobdata.PUBLISHED_VIDEO
        }

        for (i = 0; i < jobs.length; i++) {

            icon = "fas fa-spinner fa-spin";
            if (jobs[i].status === "COMPLETED") {
                icon = "fas fa-check";
            } else if (jobs[i].status === "FAILED") {
                icon = "fas fa-times";
            }

            let youtube_url = "";
            if (jobs[i].videoId === "INVALID") {
                youtube_url = "n/A";
            } else {
                youtube_url = "https://www.youtube.com/watch?v=" + jobs[i].videoId;
            }


            $("#tbody").append(`<tr><th scope="row">${jobs[i].id}</th><td>${jobs[i].title}</td>
                                                                      <td>${getDetailedStatus(jobs[i].detailedStatus)}</td>
                                                                        <td>${getYoutubeLink(jobs[i].videoId)}</td>
                                                                          <td><i class="${icon}"></i></td>
                                                                      </tr>`);
            $("#tasks").slideDown();
        }
    }

    function getYoutubeLink(videoId) {
        let youtube_url = "";
        if (jobs[i].videoId === "INVALID") {
            youtube_url = "n/A";
        } else {
            youtube_url = "https://www.youtube.com/watch?v=" + jobs[i].videoId;
        }

        return youtube_url;
    }

    function getDetailedStatus(batchStatus) {
        let parsedStatus = batchStatus;

        switch (parsedStatus) {
            case "PARSING_CONTENT":
                parsedStatus = "Parsing content";
                break;
            case "CREATING_AUDIO":
                parsedStatus = "Creating audio";
                break;
            case "CREATING_VIDEO":
                parsedStatus = "Creating video";
                break;
            case "UPLOADING_VIDEO":
                parsedStatus = "Uploading Video";
                break;
            case "PUBLISHED":
                parsedStatus = "Published";
                break;
        }
        return parsedStatus;
    }


    setInterval(function () {
        updateTable();
    }, 1000);

});


